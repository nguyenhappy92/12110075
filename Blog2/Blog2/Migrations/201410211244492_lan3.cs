namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
        }
    }
}
