﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [Range(10, 100)]
        public string Content { set; get; }
      

        public virtual ICollection<Post> Posts { set; get; }
    }
}