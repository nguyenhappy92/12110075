﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        [StringLength(50,ErrorMessage="Tối thiểu 50 kí tự")]
        public String Body { set; get; }
        
        [Required]
        [DataType(DataType.Date)]
        [Range(5, 15)]
        public DateTime DateCreated { set; get; }
      
        [Required]
        [DataType(DataType.Date)]
        [Range(5, 15)]
        public DateTime DateUpdated { set; get; }
         
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}