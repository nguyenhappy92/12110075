﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
     [Table("BaiViet")]
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Số lượng kí tự từ 20 - 50", MinimumLength = 20)]
        public String Title { set; get; }
       
        [Required]
        [StringLength(50,ErrorMessage="Tối thiểu 50 kí tự")]
        public String Body { set; get; }
        
        [Required]
        [DataType(DataType.Date)]
        [Range(5, 15)]
        public DateTime DateCreated { set; get; }
         [Required]
         [DataType(DataType.Date)] 
         [Range(5,16)]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

        public virtual Account Account { set; get; }
    }
}