﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public int Password { set; get; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(100, ErrorMessage = "Email không hợp lệ")]
        public string Email { set; get; }
       
        [Required]
        [StringLength(100, ErrorMessage = "Số lượng ký tự từ 10-100", MinimumLength = 10)]
        public string FirstName { set; get; }
         
         [Required]
         [StringLength(100, ErrorMessage = "Số lượng ký tự từ 10-100", MinimumLength = 10)]
        public string LastName { set; get; }
        
        public virtual ICollection<Post> Posts { set; get; }
    }
}