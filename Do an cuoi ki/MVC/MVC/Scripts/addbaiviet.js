﻿$(function () {
    $("#baiviet").submit(function (e) {
        e.preventDefault();
        $.ajax({

            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (data) {
                $("#result").prepend(data);
            }
        });
        $("#baiviet").trigger("reset")
    });
});