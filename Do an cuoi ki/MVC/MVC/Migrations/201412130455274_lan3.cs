namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViets", "NoiDungBV", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViets", "NoiDungBV", c => c.String(nullable: false, maxLength: 1500));
        }
    }
}
