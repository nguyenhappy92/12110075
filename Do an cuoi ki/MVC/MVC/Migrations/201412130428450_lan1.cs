namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.BaiViets",
                c => new
                    {
                        MaBV = c.Int(nullable: false, identity: true),
                        TenBV = c.String(nullable: false, maxLength: 100),
                        TomTatNoiDung = c.String(nullable: false, maxLength: 200),
                        NoiDungBV = c.String(nullable: false, maxLength: 1500),
                        HinhAnhBV = c.Binary(),
                        NgayTao = c.DateTime(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.MaBV)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        MaBL = c.Int(nullable: false, identity: true),
                        NoDungBL = c.String(nullable: false, maxLength: 500),
                        NgayTao = c.DateTime(nullable: false),
                        BaiVietMaBV = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaBL)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietMaBV, cascadeDelete: true)
                .Index(t => t.BaiVietMaBV);
            
            CreateTable(
                "dbo.TuKhoas",
                c => new
                    {
                        MaTK = c.Int(nullable: false, identity: true),
                        TenTK = c.String(),
                    })
                .PrimaryKey(t => t.MaTK);
            
            CreateTable(
                "dbo.DiemBaiViets",
                c => new
                    {
                        MaDiem = c.Int(nullable: false, identity: true),
                        SoDiem = c.Int(nullable: false),
                        BaiVietMD = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaDiem)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietMD, cascadeDelete: true)
                .Index(t => t.BaiVietMD);
            
            CreateTable(
                "dbo.DanhGiaBaiViets",
                c => new
                    {
                        MaDG = c.Int(nullable: false, identity: true),
                        LoaiDanhGia = c.String(),
                        BieuTuong = c.Binary(),
                        DanhGiaBV = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaDG)
                .ForeignKey("dbo.BaiViets", t => t.DanhGiaBV, cascadeDelete: true)
                .Index(t => t.DanhGiaBV);
            
            CreateTable(
                "dbo.LoaiChuDes",
                c => new
                    {
                        MaLoaiCD = c.Int(nullable: false, identity: true),
                        TenLoaiCD = c.String(),
                        ChuDeMaCD = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaLoaiCD)
                .ForeignKey("dbo.ChuDes", t => t.ChuDeMaCD, cascadeDelete: true)
                .Index(t => t.ChuDeMaCD);
            
            CreateTable(
                "dbo.ChuDes",
                c => new
                    {
                        MaCD = c.Int(nullable: false, identity: true),
                        TenChuDe = c.String(),
                    })
                .PrimaryKey(t => t.MaCD);
            
            CreateTable(
                "dbo.TuMaBV",
                c => new
                    {
                        BaiVietMaBV = c.Int(nullable: false),
                        MaTK = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BaiVietMaBV, t.MaTK })
                .ForeignKey("dbo.BaiViets", t => t.BaiVietMaBV, cascadeDelete: true)
                .ForeignKey("dbo.TuKhoas", t => t.MaTK, cascadeDelete: true)
                .Index(t => t.BaiVietMaBV)
                .Index(t => t.MaTK);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TuMaBV", new[] { "MaTK" });
            DropIndex("dbo.TuMaBV", new[] { "BaiVietMaBV" });
            DropIndex("dbo.LoaiChuDes", new[] { "ChuDeMaCD" });
            DropIndex("dbo.DanhGiaBaiViets", new[] { "DanhGiaBV" });
            DropIndex("dbo.DiemBaiViets", new[] { "BaiVietMD" });
            DropIndex("dbo.BinhLuans", new[] { "BaiVietMaBV" });
            DropIndex("dbo.BaiViets", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.TuMaBV", "MaTK", "dbo.TuKhoas");
            DropForeignKey("dbo.TuMaBV", "BaiVietMaBV", "dbo.BaiViets");
            DropForeignKey("dbo.LoaiChuDes", "ChuDeMaCD", "dbo.ChuDes");
            DropForeignKey("dbo.DanhGiaBaiViets", "DanhGiaBV", "dbo.BaiViets");
            DropForeignKey("dbo.DiemBaiViets", "BaiVietMD", "dbo.BaiViets");
            DropForeignKey("dbo.BinhLuans", "BaiVietMaBV", "dbo.BaiViets");
            DropForeignKey("dbo.BaiViets", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.TuMaBV");
            DropTable("dbo.ChuDes");
            DropTable("dbo.LoaiChuDes");
            DropTable("dbo.DanhGiaBaiViets");
            DropTable("dbo.DiemBaiViets");
            DropTable("dbo.TuKhoas");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.BaiViets");
            DropTable("dbo.UserProfile");
        }
    }
}
