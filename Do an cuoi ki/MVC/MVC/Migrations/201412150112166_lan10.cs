namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoaiTroChois",
                c => new
                    {
                        MaLoaiTC = c.Int(nullable: false, identity: true),
                        MucDoTC = c.String(),
                    })
                .PrimaryKey(t => t.MaLoaiTC);
            
            CreateTable(
                "dbo.TroChois",
                c => new
                    {
                        MaTC = c.Int(nullable: false, identity: true),
                        TenTC = c.String(),
                        HuongDanTC = c.String(),
                        VideoHD = c.Binary(),
                        LoaiTroChoiMaLoaiTC = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaTC)
                .ForeignKey("dbo.LoaiTroChois", t => t.LoaiTroChoiMaLoaiTC, cascadeDelete: true)
                .Index(t => t.LoaiTroChoiMaLoaiTC);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TroChois", new[] { "LoaiTroChoiMaLoaiTC" });
            DropForeignKey("dbo.TroChois", "LoaiTroChoiMaLoaiTC", "dbo.LoaiTroChois");
            DropTable("dbo.TroChois");
            DropTable("dbo.LoaiTroChois");
        }
    }
}
