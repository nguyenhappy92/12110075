namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoaiTaiKhoans",
                c => new
                    {
                        MaLoaiTK = c.Int(nullable: false, identity: true),
                        CapDo = c.String(),
                        DiemTL = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MaLoaiTK);
            
            AddColumn("dbo.UserProfile", "LoaiTaiKhoanUserId", c => c.Int(nullable: false));
            AddColumn("dbo.UserProfile", "LoaiTaiKhoan_MaLoaiTK", c => c.Int());
            CreateIndex("dbo.UserProfile", "LoaiTaiKhoan_MaLoaiTK");
            AddForeignKey("dbo.UserProfile", "LoaiTaiKhoan_MaLoaiTK", "dbo.LoaiTaiKhoans", "MaLoaiTK");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfile", "LoaiTaiKhoan_MaLoaiTK", "dbo.LoaiTaiKhoans");
            DropIndex("dbo.UserProfile", new[] { "LoaiTaiKhoan_MaLoaiTK" });
            DropColumn("dbo.UserProfile", "LoaiTaiKhoan_MaLoaiTK");
            DropColumn("dbo.UserProfile", "LoaiTaiKhoanUserId");
            DropTable("dbo.LoaiTaiKhoans");
        }
    }
}
