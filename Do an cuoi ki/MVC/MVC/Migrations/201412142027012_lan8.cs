namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan8 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViets", "TomTatNoiDung", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViets", "TomTatNoiDung", c => c.String(nullable: false, maxLength: 200));
        }
    }
}
