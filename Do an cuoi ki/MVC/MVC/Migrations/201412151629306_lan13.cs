namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaiViets", "ChuDeMaBV", c => c.Int(nullable: false));
            AddColumn("dbo.BaiViets", "ChuDe_MaCD", c => c.Int());
            CreateIndex("dbo.BaiViets", "ChuDe_MaCD");
            AddForeignKey("dbo.BaiViets", "ChuDe_MaCD", "dbo.ChuDes", "MaCD");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaiViets", "ChuDe_MaCD", "dbo.ChuDes");
            DropIndex("dbo.BaiViets", new[] { "ChuDe_MaCD" });
            DropColumn("dbo.BaiViets", "ChuDe_MaCD");
            DropColumn("dbo.BaiViets", "ChuDeMaBV");
        }
    }
}
