namespace MVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LienKetWebs",
                c => new
                    {
                        MaLK = c.Int(nullable: false, identity: true),
                        TenWeb = c.String(),
                        Link = c.String(),
                    })
                .PrimaryKey(t => t.MaLK);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LienKetWebs");
        }
    }
}
