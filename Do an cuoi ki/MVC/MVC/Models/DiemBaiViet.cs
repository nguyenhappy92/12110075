﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class DiemBaiViet
    {
        [Key]
        public int MaDiem { set; get; }
        public int SoDiem { set; get; }
        public int BaiVietMD { set; get; }
        public virtual BaiViet BaiViet { set; get; }
    }
}