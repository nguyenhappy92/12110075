﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class ChuDe
    {
        [Key]
        public int MaCD { set; get; }
        public String TenChuDe { set; get; }
        public virtual ICollection<LoaiChuDe> LoaiChuDes { set; get; }
        public virtual ICollection<BaiViet> BaiViets { set; get; }
    }
}