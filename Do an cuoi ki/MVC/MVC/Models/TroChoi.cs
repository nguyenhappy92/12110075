﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TroChoi
    {
        [Key]
        public int MaTC { set; get; }
        public String TenTC { set; get; }
        public String HuongDanTC { set; get; }
        public byte[] VideoHD { set; get; }
        public int LoaiTroChoiMaLoaiTC { set; get; }
        public virtual LoaiTroChoi LoaiTroChoi { set; get; }
    }
}