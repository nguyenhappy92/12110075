﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class LoaiChuDe
    {
        [Key]
        public int MaLoaiCD { set; get; }
        public String TenLoaiCD { set; get; }
        public int ChuDeMaCD { set; get; }
        public virtual ChuDe ChuDe { set; get; }
    }
}