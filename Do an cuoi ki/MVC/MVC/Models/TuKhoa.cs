﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TuKhoa
    {
        [Key]
        public int MaTK { set; get; }
        public String TenTK { set; get; }
        public virtual ICollection<BaiViet> BaiViets { set; get; }
    }
}