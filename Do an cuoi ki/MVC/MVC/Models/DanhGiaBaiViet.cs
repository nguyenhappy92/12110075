﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class DanhGiaBaiViet
    {
        [Key]
        public int MaDG { set; get; }
        public String LoaiDanhGia { set; get; }
        public byte[] BieuTuong { set; get; }
        public int DanhGiaBV { set; get; }
        public virtual BaiViet BaiViet { set; get; }
    }
}