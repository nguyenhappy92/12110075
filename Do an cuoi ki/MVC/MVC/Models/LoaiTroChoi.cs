﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class LoaiTroChoi
    {
        [Key]
        public int MaLoaiTC { set; get; }
        public String MucDoTC { set; get; }
        public virtual ICollection<TroChoi> Trochois { set; get; }
    }
}