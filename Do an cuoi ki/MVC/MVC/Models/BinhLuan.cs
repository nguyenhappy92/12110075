﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class BinhLuan
    {
        [Key]
        public int MaBL { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Sô lượng kí tự tối thiểu 20", MinimumLength = 50)]
        public String NoDungBL { set; get; }
        [Required(ErrorMessage = "Nhập ngày hợp lệ]")]
        [DataType(DataType.Date)]
        public DateTime NgayTao { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - NgayTao).Minutes;
            }
        }
        public int BaiVietMaBV { set; get; }

        public virtual BaiViet BaiViet { set; get; }
    }
}