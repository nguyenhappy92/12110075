﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class TroGiup
    {
        [Key]
        public int MaTG { get; set; }
        public string HoTen { get; set; }
        public string DiaChi { get; set; }
        public string SoDT { get; set; }
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Nhập Email hợp lệ")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Sô lượng kí tự tối thiểu 100", MinimumLength = 20)]
        public string ChuDe { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(1500, ErrorMessage = "Sô lượng kí tự tối thiểu 100", MinimumLength = 100)]
        public string NoiDung { get; set; }
    }
}