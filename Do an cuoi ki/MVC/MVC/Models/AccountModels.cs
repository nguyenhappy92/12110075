﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace MVC.Models
{
    public class MVCDbContext : DbContext
    {
        public MVCDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<BaiViet>()
                .HasMany(d => d.TuKhoas).WithMany(p => p.BaiViets)
                .Map(t => t.MapLeftKey("BaiVietMaBV").MapRightKey("MaTK").ToTable("TuMaBV"));
             //modelBuilder.Entity<CaNhan>().HasKey(t => t.MaCN);
             //modelBuilder.Entity<CaNhan>().HasRequired(t => t.UserProflie)
             //    .WithRequiredPrincipal(t => t.CaNhan);
            modelBuilder.Entity<DanhGiaBaiViet>().HasRequired<BaiViet>(s => s.BaiViet)
                 .WithMany(s => s.DanhGiaBaiViets).HasForeignKey(s => s.DanhGiaBV);
            modelBuilder.Entity<DiemBaiViet>().HasRequired<BaiViet>(s => s.BaiViet)
              .WithMany(s => s.DiemBaiViets).HasForeignKey(s => s.BaiVietMD);
            /* modelBuilder.Entity<BaiViet>().HasRequired<TaiKhoan>(d => d.TaiKhoan)
                 .WithMany(d => d.BaiViets).HasForeignKey(d => d.TaiKhoanMaTK);
             /*modelBuilder.Entity<BinhLuan>().HasRequired<TaiKhoan>(e => e.TaiKhoan)
                 .WithMany(e => e.BinhLuans).HasForeignKey(e => e.TKMaTK);*/
            /*modelBuilder.Entity<TaiKhoan>().HasRequired<LoaiTaiKhoan>(g => g.LoaiTaiKhoan)
                .WithMany(g => g.TaiKhoans).HasForeignKey(g => g.LoaiTaiKhoanMaLoaiTk);*/
           /* modelBuilder.Entity<TroChoi>().HasRequired<LoaiTroChoi>(e => e.LoaiTroChoi)
                .WithMany(e => e.Trochois).HasForeignKey(e => e.LoaiTroChoiMaLoaiTC);*/
            modelBuilder.Entity<LoaiChuDe>().HasRequired<ChuDe>(n => n.ChuDe)
                .WithMany(n => n.LoaiChuDes).HasForeignKey(n => n.ChuDeMaCD);
            modelBuilder.Entity<BinhLuan>().HasRequired<BaiViet>(c => c.BaiViet)
                .WithMany(c => c.BinhLuans).HasForeignKey(c => c.BaiVietMaBV);
        }

        public DbSet<BaiViet> BaiViets { get; set; }
        public DbSet<BinhLuan> BinhLuans { get; set; }
        public DbSet<TuKhoa> TuKhoas { get; set; }
        public DbSet<DanhGiaBaiViet> DanhGiaBaiViets { get; set; }
        public DbSet<DiemBaiViet> DiemBaiViets { get; set; }

        public DbSet<LienHe> LienHes { get; set; }

        public DbSet<TroGiup> TroGiups { get; set; }

        public DbSet<LienKetWeb> LienKetWebs { get; set; }
        
       public DbSet<LoaiChuDe> LoaiChuDes { get; set; }
       public DbSet<LoaiTroChoi> LoaiTroChois { get; set; }
        public DbSet<TroChoi> TroChois { get; set; }
        public DbSet<LoaiTaiKhoan>LoaiTaiKhoans{get;set;}
        //public DbSet<CaNhan> CaNhans { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public virtual ICollection<BaiViet> BaiViets { get; set; }
        public int LoaiTaiKhoanUserId { get; set; }
        public virtual LoaiTaiKhoan LoaiTaiKhoan { get; set; }
        //public virtual CaNhan CaNhan { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Mật khẩu tối thiểu là 6 kí tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu chưa phù hợp")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ mật khẩu?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Mật khẩu tối thiểu là 6 kí tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Mật khẩu chưa phù hợp")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
