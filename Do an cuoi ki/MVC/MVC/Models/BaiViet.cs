﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class BaiViet
    {
    
        [Key]
        public int MaBV { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Sô lượng kí tự tối thiểu 20", MinimumLength = 20)]
        public String TenBV { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        //[StringLength(200, ErrorMessage = "Sô lượng kí tự tối thiểu 20", MinimumLength = 50)]
        public String TomTatNoiDung { set; get; }
        [Required(ErrorMessage="Không được bỏ trống")]
        [StringLength(99999,ErrorMessage="Sô lượng kí tự tối thiểu 100",MinimumLength=100)]
        public String NoiDungBV { set; get; }
        public byte[] HinhAnhBV { set; get; }
        [Required(ErrorMessage="Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime)]
        public DateTime NgayTao { set; get; }
       
        public int UserProfileId { set; get; }
        public virtual ICollection<BinhLuan> BinhLuans { set; get; }
        public virtual ICollection<TuKhoa> TuKhoas { set; get; }
        public virtual ICollection<DiemBaiViet> DiemBaiViets { set; get; }
        public virtual ICollection< DanhGiaBaiViet> DanhGiaBaiViets { set; get; }
        public virtual UserProfile UserProfile { set;get;}
        public int ChuDeMaBV { set; get; }
        public virtual ChuDe ChuDe { set; get; }
    }
}