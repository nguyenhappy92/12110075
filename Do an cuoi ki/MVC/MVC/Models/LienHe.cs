﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class LienHe
    {
        [Key]
        public int MaLH { set; get; }
        public String HoTen { set; get; }
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email phải hợp lệ")]
        public String Email { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Sô lượng kí tự tối thiểu 20", MinimumLength = 50)]
        public String NoiDung { set; get; }
    }
}