﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class LoaiTaiKhoan
    {
        [Key]
        public int MaLoaiTK { set; get; }
        public String CapDo { set; get; }
        public int DiemTL { set; get; }
        public ICollection<UserProfile> UserProflies { set; get; }
    }
}