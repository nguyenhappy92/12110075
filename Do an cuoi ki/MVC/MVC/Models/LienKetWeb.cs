﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models
{
    public class LienKetWeb
    {
        [Key]
        public int MaLK { set; get; }
        public String TenWeb { set; get; }
        public String Link { set; get; }
    }
}