﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class LienHeController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /LienHe/

        public ActionResult Index()
        {
            return View(db.LienHes.ToList());
        }

        //
        // GET: /LienHe/Details/5

        public ActionResult Details(int id = 0)
        {
            LienHe lienhe = db.LienHes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        //
        // GET: /LienHe/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LienHe/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LienHe lienhe)
        {
            if (ModelState.IsValid)
            {
                db.LienHes.Add(lienhe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lienhe);
        }

        //
        // GET: /LienHe/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LienHe lienhe = db.LienHes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        //
        // POST: /LienHe/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LienHe lienhe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lienhe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lienhe);
        }

        //
        // GET: /LienHe/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LienHe lienhe = db.LienHes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        //
        // POST: /LienHe/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LienHe lienhe = db.LienHes.Find(id);
            db.LienHes.Remove(lienhe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}