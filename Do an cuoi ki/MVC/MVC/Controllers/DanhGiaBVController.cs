﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class DanhGiaBVController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /DanhGiaBV/

        public ActionResult Index()
        {
            var danhgiabaiviets = db.DanhGiaBaiViets.Include(d => d.BaiViet);
            return View(danhgiabaiviets.ToList());
        }

        //
        // GET: /DanhGiaBV/Details/5

        public ActionResult Details(int id = 0)
        {
            DanhGiaBaiViet danhgiabaiviet = db.DanhGiaBaiViets.Find(id);
            if (danhgiabaiviet == null)
            {
                return HttpNotFound();
            }
            return View(danhgiabaiviet);
        }

        //
        // GET: /DanhGiaBV/Create

        public ActionResult Create()
        {
            ViewBag.DanhGiaBV = new SelectList(db.BaiViets, "MaBV", "TenBV");
            return View();
        }

        //
        // POST: /DanhGiaBV/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhGiaBaiViet danhgiabaiviet)
        {
            if (ModelState.IsValid)
            {
                db.DanhGiaBaiViets.Add(danhgiabaiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DanhGiaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", danhgiabaiviet.DanhGiaBV);
            return View(danhgiabaiviet);
        }

        //
        // GET: /DanhGiaBV/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DanhGiaBaiViet danhgiabaiviet = db.DanhGiaBaiViets.Find(id);
            if (danhgiabaiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.DanhGiaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", danhgiabaiviet.DanhGiaBV);
            return View(danhgiabaiviet);
        }

        //
        // POST: /DanhGiaBV/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhGiaBaiViet danhgiabaiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danhgiabaiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DanhGiaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", danhgiabaiviet.DanhGiaBV);
            return View(danhgiabaiviet);
        }

        //
        // GET: /DanhGiaBV/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DanhGiaBaiViet danhgiabaiviet = db.DanhGiaBaiViets.Find(id);
            if (danhgiabaiviet == null)
            {
                return HttpNotFound();
            }
            return View(danhgiabaiviet);
        }

        //
        // POST: /DanhGiaBV/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DanhGiaBaiViet danhgiabaiviet = db.DanhGiaBaiViets.Find(id);
            db.DanhGiaBaiViets.Remove(danhgiabaiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}