﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    [Authorize]
    public class BaiVietController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /BaiViet/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.BaiViets.ToList());
        }

        //
        // GET: /BaiViet/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            ViewData["idbaiviet"] = id;
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BaiViet/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BaiViet baiviet,string TenTK)
        {
            if (ModelState.IsValid)
            {
                baiviet.NgayTao = DateTime.Now;
                int userid = db.UserProfiles.Select(x=>new{x.UserId,x.UserName})
                    .Where(y=>y.UserName==User.Identity.Name).Single().UserId;
                //User.Identity.Name
                //Gan id vao cho post
                //int userid=db.UserProfiles.select(j =>new {j.us})

               /* int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                       .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                baiviet.UserProfileId = userid;*/

                //Tao list cac Tag
                List<TuKhoa> TuKhoas = new List<TuKhoa>();
                //Tach cac Tag theo dau ,
                string[] TagContent =TenTK.Split(',');
                //Lap cac Tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    TuKhoa tagExits = null;
                    var ListTag = db.TuKhoas.Where(y => y.TenTK.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //Neu co Tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.BaiViets.Add(baiviet);
                    }
                    else
                    {
                        //neu chua co Tag thi tao moi
                        tagExits = new TuKhoa();
                        tagExits.TenTK = item;
                        tagExits.BaiViets = new List<BaiViet>();
                        tagExits.BaiViets.Add(baiviet);
                    }
                    //add vao List cac Tag
                    TuKhoas.Add(tagExits);
                }
                //Gan List Tag cho Post
                baiviet.TuKhoas = TuKhoas;
                //
                baiviet.UserProfileId = userid;
                db.BaiViets.Add(baiviet);
                db.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(baiviet);
        }

        //
        // GET: /BaiViet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BaiViet baiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            db.BaiViets.Remove(baiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}