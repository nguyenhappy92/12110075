﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class TroGiupController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /TroGiup/

        public ActionResult Index()
        {
            return View(db.TroGiups.ToList());
        }

        //
        // GET: /TroGiup/Details/5

        public ActionResult Details(int id = 0)
        {
            TroGiup trogiup = db.TroGiups.Find(id);
            if (trogiup == null)
            {
                return HttpNotFound();
            }
            return View(trogiup);
        }

        //
        // GET: /TroGiup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TroGiup/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TroGiup trogiup)
        {
            if (ModelState.IsValid)
            {
                db.TroGiups.Add(trogiup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trogiup);
        }

        //
        // GET: /TroGiup/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TroGiup trogiup = db.TroGiups.Find(id);
            if (trogiup == null)
            {
                return HttpNotFound();
            }
            return View(trogiup);
        }

        //
        // POST: /TroGiup/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TroGiup trogiup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trogiup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trogiup);
        }

        //
        // GET: /TroGiup/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TroGiup trogiup = db.TroGiups.Find(id);
            if (trogiup == null)
            {
                return HttpNotFound();
            }
            return View(trogiup);
        }

        //
        // POST: /TroGiup/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TroGiup trogiup = db.TroGiups.Find(id);
            db.TroGiups.Remove(trogiup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}