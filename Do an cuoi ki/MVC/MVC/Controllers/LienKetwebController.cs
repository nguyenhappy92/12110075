﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class LienKetwebController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /LienKetweb/

        public ActionResult Index()
        {
            return View(db.LienKetWebs.ToList());
        }

        //
        // GET: /LienKetweb/Details/5

        public ActionResult Details(int id = 0)
        {
            LienKetWeb lienketweb = db.LienKetWebs.Find(id);
            if (lienketweb == null)
            {
                return HttpNotFound();
            }
            return View(lienketweb);
        }

        //
        // GET: /LienKetweb/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LienKetweb/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LienKetWeb lienketweb)
        {
            if (ModelState.IsValid)
            {
                db.LienKetWebs.Add(lienketweb);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lienketweb);
        }

        //
        // GET: /LienKetweb/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LienKetWeb lienketweb = db.LienKetWebs.Find(id);
            if (lienketweb == null)
            {
                return HttpNotFound();
            }
            return View(lienketweb);
        }

        //
        // POST: /LienKetweb/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LienKetWeb lienketweb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lienketweb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lienketweb);
        }

        //
        // GET: /LienKetweb/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LienKetWeb lienketweb = db.LienKetWebs.Find(id);
            if (lienketweb == null)
            {
                return HttpNotFound();
            }
            return View(lienketweb);
        }

        //
        // POST: /LienKetweb/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LienKetWeb lienketweb = db.LienKetWebs.Find(id);
            db.LienKetWebs.Remove(lienketweb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}