﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class DiemBaVController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /DiemBaV/

        public ActionResult Index()
        {
            var diembaiviets = db.DiemBaiViets.Include(d => d.BaiViet);
            return View(diembaiviets.ToList());
        }

        //
        // GET: /DiemBaV/Details/5

        public ActionResult Details(int id = 0)
        {
            DiemBaiViet diembaiviet = db.DiemBaiViets.Find(id);
            if (diembaiviet == null)
            {
                return HttpNotFound();
            }
            return View(diembaiviet);
        }

        //
        // GET: /DiemBaV/Create

        public ActionResult Create()
        {
            ViewBag.BaiVietMD = new SelectList(db.BaiViets, "MaBV", "TenBV");
            return View();
        }

        //
        // POST: /DiemBaV/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DiemBaiViet diembaiviet)
        {
            if (ModelState.IsValid)
            {
                db.DiemBaiViets.Add(diembaiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BaiVietMD = new SelectList(db.BaiViets, "MaBV", "TenBV", diembaiviet.BaiVietMD);
            return View(diembaiviet);
        }

        //
        // GET: /DiemBaV/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DiemBaiViet diembaiviet = db.DiemBaiViets.Find(id);
            if (diembaiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiVietMD = new SelectList(db.BaiViets, "MaBV", "TenBV", diembaiviet.BaiVietMD);
            return View(diembaiviet);
        }

        //
        // POST: /DiemBaV/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DiemBaiViet diembaiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(diembaiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiVietMD = new SelectList(db.BaiViets, "MaBV", "TenBV", diembaiviet.BaiVietMD);
            return View(diembaiviet);
        }

        //
        // GET: /DiemBaV/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DiemBaiViet diembaiviet = db.DiemBaiViets.Find(id);
            if (diembaiviet == null)
            {
                return HttpNotFound();
            }
            return View(diembaiviet);
        }

        //
        // POST: /DiemBaV/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DiemBaiViet diembaiviet = db.DiemBaiViets.Find(id);
            db.DiemBaiViets.Remove(diembaiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}