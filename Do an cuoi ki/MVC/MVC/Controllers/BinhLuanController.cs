﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class BinhLuanController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /BinhLuan/

        public ActionResult Index()
        {
            var binhluans = db.BinhLuans.Include(b => b.BaiViet);
            return View(binhluans.ToList());
        }

        //
        // GET: /BinhLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Create

        public ActionResult Create()
        {
            ViewBag.BaiVietMaBV = new SelectList(db.BaiViets, "MaBV", "TenBV");
            return View();
        }

        //
        // POST: /BinhLuan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BinhLuan binhluan,int idbaiviet)
        {
            if (ModelState.IsValid)
            {
                binhluan.BaiVietMaBV = idbaiviet;
                binhluan.NgayTao = DateTime.Now;
                db.BinhLuans.Add(binhluan);
                db.SaveChanges();
                //return PartialView("Details/"+idbaiviet,"BaiViet");
                return PartialView("_addAjax", binhluan);
            }

            ViewBag.BaiVietMaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", binhluan.BaiVietMaBV);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiVietMaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", binhluan.BaiVietMaBV);
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiVietMaBV = new SelectList(db.BaiViets, "MaBV", "TenBV", binhluan.BaiVietMaBV);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            db.BinhLuans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}