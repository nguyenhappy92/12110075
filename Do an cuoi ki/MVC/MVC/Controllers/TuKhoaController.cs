﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC.Models;

namespace MVC.Controllers
{
    public class TuKhoaController : Controller
    {
        private MVCDbContext db = new MVCDbContext();

        //
        // GET: /TuKhoa/

        public ActionResult Index()
        {
            return View(db.TuKhoas.ToList());
        }

        //
        // GET: /TuKhoa/Details/5

        public ActionResult Details(int id = 0)
        {
            TuKhoa tukhoa = db.TuKhoas.Find(id);
            if (tukhoa == null)
            {
                return HttpNotFound();
            }
            return View(tukhoa);
        }

        //
        // GET: /TuKhoa/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TuKhoa/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TuKhoa tukhoa)
        {
            if (ModelState.IsValid)
            {
                db.TuKhoas.Add(tukhoa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tukhoa);
        }

        //
        // GET: /TuKhoa/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TuKhoa tukhoa = db.TuKhoas.Find(id);
            if (tukhoa == null)
            {
                return HttpNotFound();
            }
            return View(tukhoa);
        }

        //
        // POST: /TuKhoa/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TuKhoa tukhoa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tukhoa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tukhoa);
        }

        //
        // GET: /TuKhoa/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TuKhoa tukhoa = db.TuKhoas.Find(id);
            if (tukhoa == null)
            {
                return HttpNotFound();
            }
            return View(tukhoa);
        }

        //
        // POST: /TuKhoa/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TuKhoa tukhoa = db.TuKhoas.Find(id);
            db.TuKhoas.Remove(tukhoa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}