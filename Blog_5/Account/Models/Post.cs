﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class Post
    {
        public List<Tag> Tag;

        public int ID { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự nằm trong khoảng từ 20 đến 500", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(111111111, ErrorMessage = "Số lượng ký tự tối thiểu là 50 ký tự", MinimumLength = 50)]
        public String Body { set; get; }

        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ!")]
        public DateTime DateCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

        public virtual UserProfile userProfile { set; get; }
        public int UserProfileUserId { set; get; }


        

       
    }
}