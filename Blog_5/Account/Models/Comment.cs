﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Account.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(111111111, ErrorMessage = "Số lượng kí tự tối thiểu là 50 kí tự", MinimumLength = 50)]
        public String Body { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public String Author { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public System.DateTime DateUpdated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}