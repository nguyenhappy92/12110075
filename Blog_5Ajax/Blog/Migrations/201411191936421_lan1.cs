namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "UserProfileUserID" });
            DropForeignKey("dbo.Posts", "UserProfileUserID", "dbo.UserProfile");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
