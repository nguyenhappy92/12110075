﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
         [StringLength(250, ErrorMessage = "Phải nhập từ 20 đến 250 ký tự",
             MinimumLength = 20)]
        public string Title { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [StringLength(int.MaxValue, ErrorMessage = "Phải nhập tối thiểu 50 ký tự",
          MinimumLength = 50)]
        public string Body { set; get; }
        [Required(ErrorMessage = "Mục này không được bỏ trống")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}